# TODO


## Games

##### Kirby
- [x] Gourmet Race
- [x] Green Greens

##### Trine
- [x] Trine 2 Main Theme

##### Legend of Zelda
- [ ] Windwaker Introduction
- [ ] Outset Island
- [ ] The Great Sea
- [ ] Lost Woods

##### Mario Kart
- [x] Rainbow Road (Double Dash)

##### Portal
- [ ] Still Alive

##### Street Fighter
- [ ] Guile's Theme

##### Super Smash Brothers Melee
- [ ] Main Menu

##### Super Mario Galaxy
- [ ] Observatory Theme

##### Skyrim
- [ ] Skyrim Main Theme

##### Angry Birds
- [ ] Angry Birds Title Theme

##### Tetris
- [ ] Tetris Theme A
- [ ] Tetris Theme B

##### Yoshi's Island
- [ ] Flower Garden
- [ ] Athletic Theme



## Popular Music

##### Enya
- [x] Only Time

##### Simon and Garfunkel
- [ ] Scarborough Fair
- [ ] The Sounds of Silence

##### John Denver
- [ ] Annie's Song



## Neo Classical

- [ ] Pachelbel's Canon
- [ ] Ave Maria
- [ ] Entry of the Gladiators



## Movies and TV

##### Game of Thrones
- [ ] Game of Thrones Main Theme

##### Aladdin
- [ ] A Whole New World

##### Lion King
- [ ] Circle of Life

##### Mulan
- [ ] I'll Make a Man Out of You

##### Charlie Brown
- [ ] Linus and Lucy

##### Lord of the Rings
- [ ] Concerning Hobbits

##### Jurassic Park

##### Pink Panther
- [ ] Pink Panther Theme

##### A-Team
- [ ] A-Team Main Theme

##### Pirates of the Caribbean

##### Star Wars
